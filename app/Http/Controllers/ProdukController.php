<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Jenis;
use App\Models\Warna;
use App\Models\Cart;
use App\Models\Total;
use App\Models\CartDetail;

use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Res
     *
     *ponse
     */
    public function index()
    {
        $data['produk'] = Produk::all();
        $data['keranjang'] = Cart::where('user_id', '=', Auth::user()->id)->get();
        $data['total'] = $data['keranjang']->sum('subtotal');

        return view('admin.produk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function data()
    // {
    //     $datas = Produk::with(['jenis', 'detail'])->get();
    //     // dd($datas);
    //     return view('data', compact('datas'));
    // }
    public function create()
    {
        $data['jenis'] = Jenis::all();
        $data['warna'] = Warna::all();
        return view('admin.produk.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            'spesifikasi' => 'required',
            'stok' => 'required|numeric', 
            'hpp' => 'required',
            'harga_jual' => 'required',
            'foto_produk' => 'required',
            'foto_produk.*' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $imageName = time() . '.' . $request->foto_produk->extension();
        $request->foto_produk->move(public_path() . '/image/foto_produk/', $imageName);

        Produk::create([
            'nama' => $request['nama'],
            'jenis_id' => $request['jenis_id'],
            'warna_id' => $request['warna_id'],
            'stok' => $request['stok'],
            'deskripsi' => $request['deskripsi'],
            'spesifikasi' => $request['spesifikasi'],
            'hpp' => $request['hpp'],
            'harga_jual' => $request['harga_jual'], 
            'foto_produk' => $imageName,
        ]);

        // $customMessages = [
        //     'nama.required' => 'Produk wajib diisi!',
        //     'deskripsi.required' => 'Deskrpsi wajib diisi',
        //     'spesifikasi.required' => 'Spesifikasi wajib diisi',
        //     'stok.required' => 'Stok wajib diisi',
        //     'hpp.required' => 'HPP wajib diisi!',
        //     'harga_jual.required' => 'Harga wajib diisi!',
        //     'foto_produk.required' => 'Foto wajib diisi!',
        // ];

        // $this->validate($customMessages);

        return redirect()->route('produk.index')->with('success', 'Data Produk berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['produk'] = Produk::findOrFail($id);
        return view('admin.produk.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['jenis'] = Jenis::all();
        $data['produk']= Produk::find($id);
        $data['warna'] = Warna::all();

        return view('admin.produk.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|string',
            'jenis_id' => 'required',
            'warna_id' => 'required',
            'deskripsi' => 'required',
            'spesifikasi' => 'required',
            'stok' => 'required|numeric',
            'hpp' => 'required|numeric',
            'harga_jual' => 'required|numeric',
        ]);

        $produk = Produk::find($id);

        if ($request->foto_produk){
            $imageName = time() . '.' . $request->foto_produk->extension();
            $request->foto_produk->move(public_path() . '/image/foto_produk/', $imageName);
        } else {
            $imageName = $produk->foto_produk;
        }

        $produk = $produk->update([
            'nama' => $request['nama'],
            'jenis_id' => $request['jenis_id'],
            'warna_id' => $request['warna_id'],
            'deskripsi' => $request['deskripsi'],
            'spesifikasi' => $request['spesifikasi'],
            'stok' => $request['stok'],
            'hpp' => $request['hpp'],
            'harga_jual' => $request['harga_jual'],
            'foto_produk' => $imageName,
        ]);

        return redirect()->route('produk.index')->with('success', 'Data Produk berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        $status = $produk->delete();
        if ($status){
            return 1;
        }else{
            return 0;
        }
    }
}
