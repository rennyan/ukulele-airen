<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Jenis;
use App\Models\Detail;
use App\Models\Warna;
use Illuminate\Support\Facades\Session;

class ProdukDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_produk = $request->session()->pull('id_produk', '0');
        $request->session()->put('id_produk',$id_produk);
        $data['produk'] = Detail::where('produk_id', $id_produk)->get();
        $data['det'] = Produk::find($id_produk);
        return view('admin.produk.detail', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $request->session()->put('id_produk',$request->session()->pull('id_produk','0'));
        $id_produk = Session::all();

        $data['warna'] = Warna::all();
        return view('admin.produk.form-detail', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_produk = Session::get('id_produk');
        Session::put('id_produk', $id_produk);
        // dd($request->all());
        $request->validate([
            'warna_id' => 'required',
            'stok' => 'required|numeric',
            'deskripsi' => 'required',
            'spesifikasi' => 'required',
            'foto_produk' => 'required',
            'foto_produk.*' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $imageName = time() . '.' . $request->foto_produk->extension();
        $request->foto_produk->move(public_path() . '/image/foto_produk/', $imageName);
        $create = Detail::create([
            'produk_id' => $id_produk,
            'warna_id' => $request['warna_id'],
            'stok' => $request['stok'],
            'deskripsi' => $request['deskripsi'],
            'spesifikasi' => $request['spesifikasi'],
            'foto_produk' => $imageName,
        ]);
        // dd($create);

        $customMessages = [
            'warna_id.required' => 'Warna wajib diisi!',
            'stok.required' => 'Stok wajib diisi!',
            'deskripsi.required' => 'Deskripsi wajib diisi!',
            'spesifikasi.required' => 'Spesifikasi wajib diisi!',
            'foto_produk.required' => 'Foto wajib diisi!',
        ];

        $this->validate($request, $customMessages);

        return redirect()->route('produk-detail.index')->with('success', 'Data Detail Produk berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        Session::put('id_produk', $id);
        return redirect()->route('produk-detail.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($iddetproduk)
    {
        $data['jenis'] = Jenis::all();
        $data['produk']= Detail::find($iddetproduk);
        $data['warna'] = Warna::all();
        $data['detail'] = Produk::all();

        return view('admin.produk.form-detail', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $iddetproduk)
    {
        $request->validate([
            'warna_id' => 'required',
            'stok' => 'required|numeric',
            'deskripsi' => 'required',
            'spesifikasi' => 'required',
        ]);

        // $produk = Produk::find($id);
        $produkDet = Detail::find($iddetproduk);
        
        if ($request->foto_produk){
            $imageName = time() . '.' . $request->foto_produk->extension();
            $request->foto_produk->move(public_path() . '/image/foto_produk/', $imageName);
        } else {
            $imageName = $produkDet->foto_produk;
        }
        $produkDet = $produkDet->update([
            // 'produk_id' => $request['produk_id'],
            'warna_id' => $request['warna_id'],
            'stok' => $request['stok'],
            'deskripsi' => $request['deskripsi'],
            'spesifikasi' => $request['spesifikasi'],
            'foto_produk' => $imageName,
        ]);

        $customMessages = [
            'warna_id.required' => 'Warna wajib diisi!',
            'stok.required' => 'Stok wajib diisi!',
            'deskripsi.required' => 'Deskripsi wajib diisi!',
            'spesifikasi.required' => 'Spesifikasi wajib diisi!',
            'foto_produk.required' => 'Foto wajib diisi!',
        ];


        $this->validate($request, $customMessages);

        return redirect()->route('produk-detail.index')->with('success', 'Data Detail Produk berhasil diubah');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($iddetproduk)
    {
        $detail = Detail::find($iddetproduk);
        $status = $detail->delete();
        if ($status){
            return 1;
        }else{
            return 0;
        }
    }
}
