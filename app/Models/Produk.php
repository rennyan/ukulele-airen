<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction;
Use App\Models\Jenis;
Use App\Models\Warna;
Use App\Models\Cart;

class Produk extends Model
{
    protected $guarded = ['id'];
    protected $table = 'produks';
    protected $primaryKey = 'id';

    public function jenis()
    {
        return $this->belongsTo(Jenis::class);
    }

    public function warna()
    {
        return $this->belongsTo(Warna::class);
    }

    public function cart(){
        return $this->belongsTo(Cart::class);
    }
}