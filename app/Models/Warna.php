<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warna extends Model
{
    protected $guarded = ['id'];
    protected $table = 'warnas';
    protected $primaryKey = 'id';

    public function warna()
    {
        return $this->hasMany(Produk::class, 'warna_id', 'id');
    }
}
