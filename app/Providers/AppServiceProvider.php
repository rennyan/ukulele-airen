<?php

namespace App\Providers;

use App\Models\Jenis;
use App\Models\Cart;
use App\Models\Total;
use App\Models\Warna;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Gate::define('admin', function (User $user) {
            return $user->role === 'Admin';
        });
        Gate::define('user', function (User $user) {
            return $user->role === 'User';
        });

        view()->composer('*', function ($view) {
            $view->with([
                'keranjang' => @Cart::where('user_id', '=', Auth::user()->id)->get(),
                'total' => @Total::total(),
            ]);
        });

    }
}
