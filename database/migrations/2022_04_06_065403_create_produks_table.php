<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('jenis_id')->nullable();
            $table->foreignId('warna_id')->nullable();
            $table->string('nama');
            $table->mediumText('deskripsi');
            $table->longText('spesifikasi');
            $table->Integer('stok');
            $table->Integer('hpp');
            $table->Integer('harga_jual');
            $table->string('foto_produk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
};