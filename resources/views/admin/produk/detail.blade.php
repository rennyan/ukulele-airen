@extends('admin.layouts.app')
@section('ukulele', 'active')

@section('title')
    Detail {{$produk ? ' Produk' : ''}}
@endsection
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Data Produk</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('produk.index')}}">Index</a>
                                </li>
                                <li class="breadcrumb-item active">{{($produk ? ' Detail' : '')}} Produk
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- app e-commerce details start -->
            <section id="basic-vertical-layouts">
                <div class="row">
                    <div class=" col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row my-2">
                                    <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                        <div class="d-flex align-items-center justify-content-center">
                                            <img src="{{ asset ('image/foto_produk/'. $produk->foto_produk)}}" alt="Ukulele" class="img-fluid product-img" alt="product image" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <h3>{{$produk->nama}}</h3>
                                        <span class="card-text item-company">By <a href="javascript:void(0)" class="company-name">Airen</a></span>
                                        <div class="ecommerce-details-price d-flex flex-wrap mt-1">
                                            <h4 class="item-price mr-1">Rp {{number_format($produk->harga_jual)}},00</h4>
                                            <ul class="unstyled-list list-inline pl-1 border-left">
                                                <li>⭐</li>
                                                <li>⭐</li>
                                                <li>⭐</li>
                                                <li>⭐</li>
                                                <li>⭐</li>
                                            </ul>
                                        </div>
                                        <p class="card-text">{{$produk->warna->warna}} - <span class="text-success">{{$produk->jenis->jenis}}</span></p>
                                        <p class="card-text">
                                            {{$produk->deskripsi}}
                                        </p>
                                        <p class="card-text"><b>Spesifikasi : </b></p>
                                        <p class="card-text">
                                            {{$produk->spesifikasi}}
                                        </p>    
                                        <ul class="product-features list-unstyled">
                                            <li><i data-feather="shopping-cart"></i> <span><b>Free Shipping</b></span></li>
                                        </ul>
                                        <hr />
                                        <div class="product-color-options">
                                            <h6>Stok tersedia : </h6>
                                            <p>
                                                {{$produk->stok}}
                                            </p>
                                        </div>
                                        <hr />
                                        <div class="transaction-item">
                                            <div class="text-left">
                                                <input type="hidden" name="user_id" value="{{ @ Auth::user()->id }}" class="user_id">
                                                <button type="button" class="btn btn-primary btn-cart btn-add-to-cart" data-id="{{$produk->id}}">
                                                    <i data-feather="shopping-cart" class="mr-50"></i>
                                                    <span class="add-to-cart">Add to cart</span>
                                                </button>
                                            </div>

                                            <br>
                                            <a href="javascript:void(0)" class="btn btn-outline-secondary btn-wishlist mr-0 mr-sm-1 mb-1 mb-sm-0">
                                                <i data-feather="heart" class="mr-50"></i>
                                                <span>Wishlist</span>
                                            </a>

                                            <div class="btn-group dropdown-icon-wrapper btn-share">
                                                <button type="button" class="btn btn-icon hide-arrow btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i data-feather="share-2"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        </div>  
    </div>
</div>                                         
                                
@endsection

@push('styles')
@endpush

@push('scripts')
<script>
$(document).ready(function () {
    $('.btn-add-to-cart').click(function () {
        // e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
            url: "{{route('cart.store')}}",
            type: "POST",
            data: {
                produk_id: id,
                _token: '{{csrf_token()}}'
            }, 
            success: function (response) {
                window.location.reload();
            }
        });
    });
    $('#cart-items').on('click', '.btn-decrease', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var qty = Number($(this).parent().find('.qty').text())-1; 
        $.ajax({
            url: "{{url('cart')}}/"+id,
            type: "POST",
            data: {
                qty: qty,
                _token: '{{csrf_token()}}',
                _method: 'PUT'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data.detail, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">' + 
                                '<img class="d-block rounded mr-1" src="{{asset("image/foto_produk/")   }}/'+d.produk.foto_produk +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                    '</div>' + 
                                
                                    '<div class="media-body">' +
                                        '<i class="ficon cart-item-remove" data-feather="x" data-id="'+d.produk.id+'"></i>' +
                                        '<div class="media-heading">' +
                                            '<h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>' +
                                        '</div>' +
                                        '<div class="btn-group" role="group">' +
                                            '<input type="hidden" class="produk_id" value="'+d.id+'">'+
                                            '<input type="hidden" name="param" value="kurang">'
                                            '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>' +
                                            '-' +
                                            '</button>' +
                                            '<button class="btn btn-outline-primary btn-sm qty" disabled="true">'
                                                +d.qty+
                                            '</button>' +
                                            '<input type="hidden" name="param" value="tambah">' +
                                            '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">' +
                                            '+' +
                                            '</button>' +
                                            '</div>' +
                                            '<h5 class="cart-item-price">Rp ' +number_format(d.subtotal)+ '</h5>' +
                                        '</div>' +
                                    '</div>';
                    })
                    $('#cart-total').html('Rp'+(response.total));
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Kuantitas produk gagal diubah', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });
    $('#cart-items').on('click', '.btn-increase', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var qty = Number($(this).parent().find('.qty').text())+1;
        // var qty = parseInt($(this).data('qty'))+1;
        $.ajax({
            url: "{{url('cart')}}/"+id,
            type: "POST",
            data: {
                qty: qty,
                _token: '{{csrf_token()}}',
                _method: 'PUT'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data.detail, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">' + 
                                '<img class="d-block rounded mr-1" src="{{asset("storage/foto")}}/'+d.produk.image +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                    '</div>' + 
                                
                                    '<div class="media-body">' +
                                        '<i class="ficon cart-item-remove" data-feather="x" data-id="'+d.produk.id+'"></i>' +
                                        '<div class="media-heading">' +
                                            '<h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>' +
                                        '</div>' +
                                        '<div class="btn-group" role="group">' +
                                            '<input type="hidden" class="produk_id" value="'+d.id+'">'+
                                            '<input type="hidden" name="param" value="kurang">'
                                            '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>' +
                                            '-' +
                                            '</button>' +
                                            '<button class="btn btn-outline-primary btn-sm qty" disabled="true">'
                                                +d.qty+
                                            '</button>' +
                                            '<input type="hidden" name="param" value="tambah">' +
                                            '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">' +
                                            '+' +
                                            '</button>' +
                                            '</div>' +
                                            '<h5 class="cart-item-price">Rp ' +number_format(d.subtotal)+ '</h5>' +
                                        '</div>' +
                                    '</div>';
                    })
                    $('#cart-total').html('Rp'+(response.total));
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Kuantitas produk gagal diubah', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });
});
</script>
@endpush