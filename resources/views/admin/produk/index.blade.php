@extends('admin.layouts.app')
@section('title', 'Data Produk')
@section('ukulele', 'active')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Produk</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
{{--                                    <li class="breadcrumb-item"><a href="{{route('jenis.index')}}">jenis</a>--}}
{{--                                    </li>--}}
                                <li class="breadcrumb-item active">Index
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
        @can('admin')
            <div class="row" id="basic-table">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('produk.create')}}" class="btn btn-primary waves-effect waves-float waves-light">Tambah Produk</a>
                        </div>
                    </div>
                </div>
                @endcan
            </div>
            <section id="card-demo-example">
                <div class="row match-height">
                    @foreach($produk as $row)
                        <div class="col-md-6 col-lg-4">
                            <div class="card">
                                <a href="{{route('produk.show',$row->id)}}"> <img src="{{ asset ('image/foto_produk/'. @$row->foto_produk)}}" alt="Ukulele" class="img-fluid product-img"></a>
                                <div class="card-body">
                                    <h4 class="card-title">{{$row->nama}}</h4></a>
                                    <p class="card-text">
                                    @can('admin')
                                    HPP : Rp {{number_format($row->hpp)}},00<br>
                                    Hargal Jual : Rp {{number_format($row->harga_jual)}},00
                                    @endcan
                                    @can('user')
                                    Harga : Rp {{number_format($row->harga_jual)}},00<br>
                                    ⭐⭐⭐⭐⭐
                                    @endcan
                                    </p>
                                    @can('admin')
                                    <a href="{{route('produk.show', $row->id)}}" class="btn btn-primary waves-effect waves-float waves-light">Detail</a>
                                    <a href="{{route('produk.edit', $row->id)}}" class="btn btn-warning waves-effect waves-float waves-light">Edit</a>
                                    <a href="#" data-id="{{$row->id}}" class="btn btn-danger btn-del waves-effect waves-float waves-light">Delete</a>
                                    @endcan

                                    @can('user')
                                    <a href="{{route('produk.show', $row->id)}}" class="btn btn-primary waves-effect waves-float waves-light">View More</a>
                                    @endcan
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
            </section>
        </div>
    </div>
</div>


@endsection

@push('styles')
@endpush

@push('scripts')
<script>
$(document).ready(function () {
    $('.btn-add-to-cart').click(function () {
        // e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
            url: "{{route('cart.store')}}",
            type: "POST",
            data: {
                product_id: id,
                _token: '{{csrf_token()}}'
            }, 
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data.cart, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                        '<img class="d-block rounded mr-1" src="{{asset("image/foto_produk/")}}/'+d.produk.foto_produk +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                    '</div>'+
                                        '<div class="media-body">'+
                                            '<i class="ficon cart-item-remove" data-feather="x" data-id="'+d.produk.id+'"></i>'+
                                            '<div class="media-heading">'+
                                            ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                            '</div>'+
                                        ' <div class="btn-group" role="group">'+
                                            '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                '<input type="hidden" name="param" value="kurang">'+
                                                '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                '</button>'+
                                                '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                '</button>'+
                                                '<input type="hidden" name="param" value="tambah">'+
                                                '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                '+</button>'+
                                                '</div>'+
                                                '<h5 class="cart-item-price">Rp'+number_format(d.subtotal)+'</h5>'+
                                        '</div>'+
                                    '</div';
                    })
                    $('#cart-total').html('Rp'+(response.total));
                    console.log(response);
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Produk Gagal Ditambahkan', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });

    $(document).on('change', '#quantity', function () {
        var id = $(this).data('id');
        var thisIs = $(this);
        $.ajax({
            url: "{{url('cart')}}/"+id,
            type: "POST",
            data: {
                qty: $(this).val(),
                _token: '{{csrf_token()}}',
                _method: 'PUT'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-total').text(response.total);
                    thisIs.parents('div.media-body').find('h6.cart-item-price').text(response.data.subtotal);
            },
            error: function (data) {
                toastr.error('Kuantitas Gagal Diubah', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            } 
        });
    });

    $('#cart-items').on('click', '.cart-item-remove', function () {
        var id = $(this).data('id');
        $.ajax({
            url: "{{url('cart')}}/"+id,
            type: "POST",
            data: {
                _token: '{{csrf_token()}}',
                _method: 'DELETE'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                        '<img class="d-block rounded mr-1" src="{{asset("image/foto_produk/")}}/'+d.produk.foto_produk +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                    '</div>'+
                                        '<div class="media-body">'+
                                            '<i class="ficon cart-item-remove" data-feather="x" data-id="'+d.produk.id+'"></i>'+
                                            '<div class="media-heading">'+
                                            ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                            '</div>'+
                                        ' <div class="btn-group" role="group">'+
                                            '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                '<input type="hidden" name="param" value="kurang">'+
                                                '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                '</button>'+
                                                '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                '</button>'+
                                                '<input type="hidden" name="param" value="tambah">'+
                                                '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                '+</button>'+
                                                '</div>'+
                                                '<h5 class="cart-item-price">Rp'+number_format(d.subtotal)+'</h5>'+
                                        '</div>'+
                                    '</div';
                    })
                    $('#cart-total').html('Rp'+(response.total)); console.log(response);
                    $('#cart-count').html((response.count));
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Produk gagal dihapus', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });


});

        $(document).ready(function () {
            $(document).on('click', '.btn-del', function () {
                var id = $(this).data('id');
                Swal.fire({
                    icon: 'error',
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                    .then((result) => {
                        if (result.value) {
                            $.ajax({
                                'url': '{{url("warna")}}/' + id,
                                'type': 'post',
                                'data': {
                                    '_method': 'DELETE',
                                    '_token': '{{csrf_token()}}'
                                },
                                success: function (response) {
                                    if (response == 1) {
                                        toastr.success('Data berhasil dihapus!', 'Berhasil!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                        location.reload();
                                    } else {
                                        toastr.error('Data gagal dihapus!', 'Gagal!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                    }
                                }
                            });
                        } else {
                            console.log(`dialog was dismissed by ${result.dismiss}`)
                        }

                    });
            });

        });

    </script>
@endpush

