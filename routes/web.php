<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Admin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Auth
// Auth::routes();

// // //Home User
// Route::get('/', [App\Http\Controllers\HomeUserController::class, 'index'])->name('/');
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('/home');

// Route::middleware('admin')->group(function () {
//     Route::resource('produk', App\Http\Controllers\ProdukController::class);    
// });

// //Start Master Admin
// Route::resource('jenis', App\Http\Controllers\JenisController::class);
// Route::resource('warna', App\Http\Controllers\WarnaController::class);

// // //Cart
// // Route::resource('cart-session', App\Http\Controllers\CartSessionController::class);

// //Cart
// Route::resource('cart', App\Http\Controllers\CartController::class);

Illuminate\Support\Facades\Auth::routes();

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('/');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
    Route::resource('produk', \App\Http\Controllers\ProdukController::class);
    Route::resource('cart', \App\Http\Controllers\CartController::class);
    Route::resource('transaksi', App\Http\Controllers\TransaksiController::class);
    Route::resource('jenis', App\Http\Controllers\JenisController::class)->except('show');
    Route::resource('warna', App\Http\Controllers\WarnaController::class)->except('show');
